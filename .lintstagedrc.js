module.exports = {
    // Type check TypeScript files
    "**/*.(ts|tsx)": () => "yarn tsc --noEmit",

    // Lint then format TypeScript and JavaScript files
    "**/*.(ts|tsx|js)": (filenames) => [
        `yarn eslint --quiet ${filenames.join(" ")}`,
    ],

    "**/*.(json)": (filenames) =>
        `yarn prettier --write ${filenames.join(" ")}`,
};

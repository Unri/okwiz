import { PrismaClient } from "@prisma/client";
const db = new PrismaClient();

async function seed() {
    const user = await db.user.create({
        data: {
            email: "yix@email.com",
        },
    });
    await db.quiz.create({
        data: {
            name: "Anime quiz",
            userId: user.id,
            createdAt: "2022-01-01T15:15:42.174Z",
            questions: {
                createMany: {
                    data: [
                        {
                            theme: "Fate",
                            statement:
                                "Quelle classe de servant n'existe pas ?",
                            type: "multiple",
                            value: 1,
                            choices: [
                                "Saber",
                                "Archer",
                                "Magician",
                                "Assassin",
                            ],
                            answers: ["Magician"],
                            createdAt: "2022-01-01T15:15:42.174Z",
                        },
                        {
                            theme: "Naruto",
                            statement: "Comment s'appelle le fils de Naruto ?",
                            type: "text",
                            value: 1,
                            answers: ["Boruto"],
                            createdAt: "2022-01-02T15:15:42.174Z",
                        },
                    ],
                },
            },
        },
    });
    await db.quiz.create({
        data: {
            name: "Culture quiz",
            userId: user.id,
        },
    });
}

seed();

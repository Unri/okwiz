import { Button, Flex, Grid, Heading, Input } from "@chakra-ui/react";
import { Form } from "remix";
import { ActionButton } from "~/components/action-button";

export default function Index() {
    return (
        <Flex
            h="100vh"
            direction="column"
            justifyContent="center"
            alignItems="center"
        >
            <ActionButton href="/quizzes" position="absolute" top="4" right="4">
                Liste des quiz
            </ActionButton>
            <Heading as="h1" size="3xl" marginBottom="12">
                Okwiz
            </Heading>
            <Form>
                <Grid gap="2">
                    <Input
                        placeholder="Code PIN du jeu"
                        textAlign="center"
                        bg="gray.700"
                        _placeholder={{ color: "gray.400" }}
                        _focus={{ borderColor: "gray.300" }}
                        borderWidth="1px"
                    />
                    <Button type="submit" isFullWidth>
                        Valider
                    </Button>
                </Grid>
            </Form>
        </Flex>
    );
}

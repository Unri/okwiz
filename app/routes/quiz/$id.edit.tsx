import { AddIcon } from "@chakra-ui/icons";
import {
    Alert,
    AlertIcon,
    Box,
    Button,
    Flex,
    FormControl,
    FormLabel,
    Input,
    Textarea,
} from "@chakra-ui/react";
import { useState } from "react";
import { Form, json, useLoaderData } from "remix";
import type { Question, Quiz } from "@prisma/client";
import type { LoaderFunction, ActionFunction } from "remix";
import { ActionButton } from "~/components/action-button";
import { EditAnswerButton } from "~/components/answer-button/edit";
import { db } from "~/utils/db.server";
import { formDataToQuestion } from "~/utils/form-data";

enum QuestionType {
    Multiple = "multiple",
    Text = "text",
}

export const action: ActionFunction = async ({ request }) => {
    const formData = await request.formData();
    const { quizId, ...question } = formDataToQuestion(formData);
    if (question.id) {
        return json(
            await db.question.update({
                data: question,
                where: {
                    id: question.id,
                },
            })
        );
    }
    return json(
        await db.question.create({
            data: {
                Quiz: {
                    connect: { id: quizId },
                },
                ...question,
            },
        })
    );
};

type LoadedData = Quiz & { questions: Question[] };
export const loader: LoaderFunction = async ({ params }) => {
    const result: LoadedData | null = await db.quiz.findUnique({
        include: {
            questions: true,
        },
        where: {
            id: params.id,
        },
    });

    result?.questions.sort(
        (a, b) =>
            new Date(a.createdAt).getDate() - new Date(b.createdAt).getDate()
    );

    return json(result);
};

export default function QuizEdit() {
    const data = useLoaderData<LoadedData | null>();

    const [selectedQuestionIndex, setSelectedQuestionIndex] = useState(0);

    if (!data) {
        return (
            <Alert status="error">
                <AlertIcon />
                Could not find this quiz
            </Alert>
        );
    }

    return (
        <Flex flexDir="column" minH="inherit">
            {(data.questions.length > 0 ? data.questions : [undefined]).map(
                (question, idx) => (
                    <Form
                        method="post"
                        style={{
                            flex: 1,
                            display:
                                idx === selectedQuestionIndex
                                    ? "initial"
                                    : "none",
                        }}
                        key={`question-${question?.id}`}
                    >
                        <input type="hidden" name="quizId" value={data.id} />
                        <input type="hidden" name="id" value={question?.id} />
                        <input type="hidden" name="value" value={1} />
                        <input
                            type="hidden"
                            name="type"
                            value={question?.type ?? "text"}
                        />
                        <Flex p={6} gap={4} flexDir="column">
                            <Button type="submit">Enregistrer</Button>
                            <FormControl>
                                <FormLabel htmlFor="theme">Thème</FormLabel>
                                <Input
                                    key={`${question?.id}-theme`}
                                    size="lg"
                                    name="theme"
                                    placeholder="Life"
                                    defaultValue={question?.theme}
                                />
                            </FormControl>

                            <FormControl isRequired>
                                <FormLabel htmlFor="statement">
                                    Question
                                </FormLabel>
                                <Input
                                    key={`${question?.id}-statement`}
                                    size="md"
                                    name="statement"
                                    placeholder="What is the meaning of life ?"
                                    defaultValue={question?.statement}
                                />
                            </FormControl>
                            {question?.type === QuestionType.Multiple && (
                                <FormControl
                                    display="grid"
                                    gridTemplateColumns="repeat(2, 1fr)"
                                    gridTemplateRows="repeat(2, 1fr)"
                                    gap={4}
                                >
                                    {question.choices.map((choice, index) => (
                                        <EditAnswerButton
                                            key={`${question.id}-choice-${index}`}
                                            id={`${question.id}-choice-${index}`}
                                            isDefaultChecked={question.answers.includes(
                                                choice
                                            )}
                                            defaultValue={choice}
                                        />
                                    ))}
                                </FormControl>
                            )}
                            {(!question ||
                                question.type === QuestionType.Text) && (
                                <FormControl isRequired>
                                    <FormLabel htmlFor="answer">
                                        Réponse
                                    </FormLabel>
                                    <Textarea
                                        key={`${question?.id}-answers`}
                                        name="answers"
                                        placeholder="42 is the answer"
                                        defaultValue={question?.answers[0]}
                                    />
                                </FormControl>
                            )}
                        </Flex>
                    </Form>
                )
            )}
            <Flex p={4} width="100%" gap={2}>
                <Flex as="ol" gap={2} flex={1} overflowX="auto">
                    {data.questions.map((_, index) => (
                        <Box as="li" key={index} listStyleType="none">
                            <Button
                                isActive={index === selectedQuestionIndex}
                                onClick={() => {
                                    setSelectedQuestionIndex(index);
                                }}
                            >
                                {index + 1}
                            </Button>
                        </Box>
                    ))}
                </Flex>
                <ActionButton icon={<AddIcon />} isRound>
                    Ajouter une question
                </ActionButton>
            </Flex>
        </Flex>
    );
}

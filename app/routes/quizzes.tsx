import { DeleteIcon, EditIcon } from "@chakra-ui/icons";
import { Flex, List, useMediaQuery } from "@chakra-ui/react";
import { useLoaderData } from "remix";
import type { Quiz, User } from "@prisma/client";
import type { LoaderFunction } from "remix";

import { ActionButton } from "~/components/action-button";
import { QuizListItem } from "~/components/quiz-list-item";
import { QuizMobileListItem } from "~/components/quiz-list-item/mobile";
import { db } from "~/utils/db.server";

type LoaderData = { quizzes: (Quiz & { User: User })[] };
export const loader: LoaderFunction = async () => {
    const data: LoaderData = {
        quizzes: await db.quiz.findMany({
            include: {
                User: true,
            },
            orderBy: {
                updatedAt: "desc",
            },
        }),
    };
    return data;
};

export default function Quizzes() {
    const data = useLoaderData<LoaderData>();
    const [isOverMobile] = useMediaQuery("(min-width: 600px)");
    const QuizItem = isOverMobile ? QuizListItem : QuizMobileListItem;
    return (
        <Flex direction="column" h="100vh">
            <ActionButton href="/" style={{ alignSelf: "flex-end" }}>
                {"Retour à l'accueil"}
            </ActionButton>
            <Flex direction="column" alignItems="center">
                <List width="100%" p={2}>
                    {data.quizzes.map(({ User: author, ...quiz }) => (
                        <QuizItem key={quiz.id} {...quiz} author={author}>
                            <ActionButton
                                href={`/quiz/${quiz.id}/edit`}
                                icon={<EditIcon />}
                            >
                                Modifier
                            </ActionButton>
                            <ActionButton
                                colorScheme="red"
                                icon={<DeleteIcon />}
                            >
                                Supprimer
                            </ActionButton>
                        </QuizItem>
                    ))}
                </List>
            </Flex>
        </Flex>
    );
}

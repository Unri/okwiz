import { formDataTo } from "~/utils/form-data";

describe("formData convertion", () => {
    it("convert to string values", () => {
        const formData = new URLSearchParams();
        formData.append("key", "value");
        expect(formDataTo()(formData)).toStrictEqual({ key: "value" });
    });

    it("convert to double values", () => {
        const formData = new URLSearchParams();
        formData.append("int", "1");
        formData.append("float", "1.6");
        expect(
            formDataTo({ numberKeys: ["int", "float"] })(formData)
        ).toStrictEqual({ int: 1.0, float: 1.6 });
    });

    it("convert to string array values", () => {
        const formData = new URLSearchParams();
        formData.append("key", "value1");
        formData.append("key", "value2");
        expect(formDataTo({ arrayKeys: ["key"] })(formData)).toEqual({
            key: ["value1", "value2"],
        });
    });

    it("convert to number array values", () => {
        const formData = new URLSearchParams();
        formData.append("float", "1.3");
        formData.append("float", "2.6");
        expect(
            formDataTo({ arrayKeys: ["float"], numberKeys: ["float"] })(
                formData
            )
        ).toEqual({
            float: [1.3, 2.6],
        });
    });
});

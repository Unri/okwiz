import type { Question } from "@prisma/client";

type ConvertResult = Record<
    string,
    FormDataEntryValue | FormDataEntryValue[] | null | number | number[] | Date
>;

type ConvertKeys = {
    arrayKeys?: string[];
    numberKeys?: string[];
};

export const formDataTo =
    <T extends ConvertResult>({
        arrayKeys = [],
        numberKeys = [],
    }: ConvertKeys = {}) =>
    (formData: FormData): T => {
        const map: ConvertResult = {};
        for (const key of formData.keys()) {
            if (arrayKeys.includes(key)) {
                map[key] = formData.getAll(key);
                if (numberKeys.includes(key)) {
                    map[key] = (map[key] as string[]).map((val) =>
                        parseFloat(val)
                    );
                }
                continue;
            }
            const value = formData.get(key);
            if (numberKeys.includes(key)) {
                map[key] = parseFloat(value as string);
            } else {
                map[key] = value;
            }
        }
        return map as T;
    };

export const formDataToQuestion = formDataTo<Question>({
    arrayKeys: ["choices", "answers"],
    numberKeys: ["value"],
});

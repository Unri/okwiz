import { extendTheme } from "@chakra-ui/react";
import type {
    ThemeConfig,
    ButtonProps,
    IconButtonProps,
} from "@chakra-ui/react";

const config: ThemeConfig = {
    initialColorMode: "dark",
    useSystemColorMode: false,
};

const theme = extendTheme({
    config,
    styles: {
        global: {
            body: {
                bg: "gray.900",
                minHeight: "100vh",
            },
        },
    },
    components: {
        Button: {
            defaultProps: {
                colorScheme: "blue",
            },
            variants: {
                solid: ({ colorScheme }: ButtonProps) => ({
                    bg: `${colorScheme}.400`,
                    _hover: { bg: `${colorScheme}.500` },
                    _active: { bg: `${colorScheme}.600` },
                    color: "inherit",
                }),
            },
        },
        IconButton: {
            defaultProps: {
                colorScheme: "blue",
            },
            variants: {
                solid: ({ colorScheme }: IconButtonProps) => ({
                    bg: `${colorScheme}.400`,
                    _hover: { bg: `${colorScheme}.500` },
                    _active: { bg: `${colorScheme}.600` },
                    color: "inherit",
                }),
            },
        },
    },
});

export default theme;

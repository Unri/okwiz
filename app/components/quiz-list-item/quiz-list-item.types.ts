import type { User, Quiz } from "@prisma/client";
import type { ReactNode } from "react";

export type QuizListItemProps = {
    author: User;
    children?: ReactNode;
} & Quiz;

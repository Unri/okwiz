import { Flex, Heading, ListItem } from "@chakra-ui/react";
import type { QuizListItemProps } from "./quiz-list-item.types";

export const QuizMobileListItem = ({ name, children }: QuizListItemProps) => (
    <Flex as={ListItem} bg="gray.700" p={4} mb={4} gap={2} alignItems="center">
        <Heading
            as="h3"
            size="md"
            flex={1}
            textOverflow="ellipsis"
            overflow="hidden"
            whiteSpace="nowrap"
        >
            {name}
        </Heading>
        {children}
    </Flex>
);

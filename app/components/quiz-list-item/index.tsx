import { Flex, Heading, ListItem, Text } from "@chakra-ui/react";
import type { QuizListItemProps } from "./quiz-list-item.types";

export const QuizListItem = ({ name, author, children }: QuizListItemProps) => (
    <Flex as={ListItem} bg="gray.700" p={4} mb={4} gap={2}>
        <Flex direction="column" flex={1}>
            <Heading as="h3" size="lg">
                {name}
            </Heading>
            <Flex alignContent="center" gap={2}>
                <Text fontSize="lg" flex={1}>
                    {author.email}
                </Text>
                {children}
            </Flex>
        </Flex>
    </Flex>
);

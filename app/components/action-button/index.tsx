import {
    Button,
    forwardRef,
    IconButton,
    useMediaQuery,
} from "@chakra-ui/react";
import { Link } from "remix";
import type { IconButtonProps } from "@chakra-ui/react";

const createLinkComponent = (href: string) =>
    forwardRef((linkProps, ref) => <Link ref={ref} {...linkProps} to={href} />);

type ActionButtonProps = {
    children: string;
    href?: string;
} & Omit<IconButtonProps, "aria-label">;

export const ActionButton = ({
    children,
    icon,
    href,
    isRound,
    ...props
}: ActionButtonProps) => {
    const [isMobile] = useMediaQuery("(max-width: 600px)");
    const ButtonComponent = isMobile && !!icon ? IconButton : Button;
    const buttonIconProps = !isMobile ? { leftIcon: icon } : { icon };
    return (
        <ButtonComponent
            {...props}
            {...buttonIconProps}
            borderRadius={isRound ? "full" : undefined}
            as={href ? createLinkComponent(href) : undefined}
            aria-label={children}
        >
            {children}
        </ButtonComponent>
    );
};

import { Checkbox, Flex, Textarea } from "@chakra-ui/react";
import { useState } from "react";

type EditAnswerButtonProps = {
    id: string;
    defaultValue: string;
    isDefaultChecked: boolean;
};

export const EditAnswerButton = ({
    id,
    defaultValue,
    isDefaultChecked,
}: EditAnswerButtonProps) => {
    const [choice, setChoice] = useState(defaultValue);
    return (
        <Flex
            bg="blue.400"
            borderRadius={6}
            padding={4}
            flexDirection="column"
            height={100}
        >
            <Checkbox
                colorScheme="green"
                iconColor="green"
                name="answers"
                alignSelf="flex-end"
                value={choice}
                id={id}
                defaultChecked={isDefaultChecked}
            />
            <Textarea
                name="choices"
                onChange={(event) => {
                    setChoice(event.target.value);
                }}
                isRequired
                value={choice}
                border="none"
                minH={0}
                flex={1}
                p={0}
                _focus={{ border: "none" }}
            />
        </Flex>
    );
};
